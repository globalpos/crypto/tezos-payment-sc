FROM ekino/ci-python:3.7-2020.05.28
LABEL maintainer="Steven Jehannet <steven.jehannet@ekino.com>"

# shadow and sudo to avoid running the container a root
# see:
# https://github.com/nodejs/docker-node/blob/master/docs/BestPractices.md
# https://github.com/mhart/alpine-node/issues/48
#
# to be able to call SmartPy either from non-root user and root (on gitlab ci)
# SmartPy path is added to PATH in /etc/profile and ENV ENV is set
#
# make to run project's make targets
# curl python3 g++ to install smartPy
# eudev-dev libusb-dev linux-headers nodejs npm for smartPy
# alpine-sdk autoconf automake git gmp-dev libtool libsodium-dev for pytezos

ARG UID=1000

RUN pip install -U pip && \
    pip install pipenv && \
    apt-get update -q -y && apt-get install -q -y libsodium-dev libsecp256k1-dev libgmp-dev sudo && \
    apt-get install -q -y curl nodejs npm && \
    curl -s https://SmartPy.io/smartpy-cli-dev/SmartPy.sh | bash -s local-install-dev /opt/SmartPy && \
    echo "export PATH=/opt/SmartPy:\$PATH" >> /etc/profile && \
    apt-get install -q -y pkg-config && \
    pip install pytezos pytest ipython fire pysodium secp256k1 'fastecdsa<2.0.0' && \
    apt-get -q -y autoremove && \
    apt-get -q -y clean && apt-get -q -y purge && \
    rm -rf /var/lib/apt/lists/* /var/lib/dpkg/*-old && \
    groupadd cetacean && \
    useradd --uid $UID mobydick && \
    mkdir /home/mobydick && \
    chown mobydick:cetacean /home/mobydick && \
    usermod -aG sudo mobydick && \
    echo 'mobydick ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
WORKDIR /home/mobydick
