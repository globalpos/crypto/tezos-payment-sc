.PHONY: help infra-clean infra-rebuild infra-shell-node infra-show-logs \
 infra-stop infra-up install-tools ligo-compile-contract ligo-compile-parameter \
 ligo-compile-storage ligo-dry-run pytezos-test python-console  smartpy-compile \
 smartpy-run smartpy-test

HOST_UID := $(shell id -u)
HOST_GID := $(shell id -g)

.DEFAULT: help # Running Make will run the help target

V = 0
Q = $(if $(filter 1,$V),,@)

# SmartPy default output dir
SMARTPY_OUTPUT=smartpy_output

# Executes a command in a running container, mainly useful to fix the terminal size on opening a shell session
#
# $(1) the user with which to enter
# $(2) the options
#
define infra-shell
	docker-compose exec -u $(1) -e COLUMNS=`tput cols` -e LINES=`tput lines` $(2)
endef

#
# Make sure to run the given command in a container identified by the given service.
# Enter into given container to execute the command only if /.dockerenv file does not exist and given Docker compose service exists.
#
# Login with -l arg to read profile file
# see https://stackoverflow.com/questions/38024160/how-to-get-etc-profile-to-run-automatically-in-alpine-docker
#
# $(1) the user with which run the command
# $(2) the Docker Compose service
# $(3) the command to run
#
define run-in-container
	if [ ! -f /.dockerenv -a "$$(docker-compose ps -q $(2) 2>/dev/null)" ]; then \
	    docker-compose exec --user $(1) $(2) sh -l -c "$(3)"; \
	elif [ $$(env|grep -c "^CI=") -gt 0 -a $$(env|grep -cw "DOCKER_DRIVER") -eq 1 ]; then \
	    docker-compose exec --user $(1) -T $(2) sh -l -c "$(3)"; \
	else \
	    $(3); \
    fi
endef

help: ## to show Help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

########################################
#              INFRA                   #
########################################
infra-clean: ## to stop and remove containers, networks, images
	$Q docker-compose down --rmi all

infra-rebuild: ## to clean and up all
	$Q make infra-clean infra-up

infra-shell-tools: ## to open a shell session in the tools container
	$Q $(call infra-shell,mobydick,tools bash)

infra-show-logs: ## to show logs from node
	$Q docker logs babylonnet_node_1 --follow

infra-stop: ## to stop all the containers
	$Q docker-compose stop

infra-up: ## to create and start all the containers
	$Q if [ ! -f .env -a -f .env.dist ]; then sed "s,#HOST_UID#,$(HOST_UID),g;s,#HOST_GID#,$(HOST_GID),g" .env.dist > .env; fi
	$Q docker-compose up --build -d

########################################
#               PYTEZOS                #
########################################
pytezos-test: ## to run tests with pytezos
	$Q $(call run-in-container,mobydick,tools,cd tests; pytest -v .)

python-console: ## to open a python console on the tools container
	$Q $(call run-in-container,mobydick,tools,python)

########################################
#               SMARTPY                #
########################################
smartpy-compile: ## to compile a Smartpy script (make smartpy-compile SCRIPT=contracts/helloTezos.py CALL=HelloTezos\(\))
	$Q f=$(basename $(notdir $(SCRIPT))) ; \
	$(call run-in-container,mobydick,tools,SmartPy.sh compile $(SCRIPT) '$(CALL)' $(SMARTPY_OUTPUT)/$$f)

smartpy-run: ## to execute a Smartpy script (make smartpy-run SCRIPT=contracts/helloTezos.py)
	$Q $(call run-in-container,mobydick,tools,SmartPy.sh run $(SCRIPT))

smartpy-test: ## to execute a Smartpy script with its tests (make smartpy-test SCRIPT=contracts/helloTezos.py)
	$Q f=$(basename $(notdir $(SCRIPT))) ; \
	$(call run-in-container,mobydick,tools,SmartPy.sh test $(SCRIPT) $(SMARTPY_OUTPUT)/$$f/test)

cleanup: ## to cleanup tmp files
	$Q rm -rf ./tmp
