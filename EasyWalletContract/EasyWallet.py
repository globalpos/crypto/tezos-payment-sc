import smartpy as sp

# EasyWallet Contrat for Point Of Sales


class EasyWallet(sp.Contract):
    # The EasyWallet constructor takes as a parameter:
    # - the initial owner address
    # - the public-key of the owner's cold-wallet
    def __init__(self,
                 initialMasterKey,
                 coldOwnerPublicKey
                 ):
        self.init(ownerKey=initialMasterKey,
                  coldOwnerKey=coldOwnerPublicKey,
                  userToWallet=sp.big_map(tkey=sp.TKeyHash, tvalue=sp.TRecord(
                      balance=sp.TMutez, nonce=sp.TNat))
                  )

    # The changeOwner function allows the creator of the contract to transfer the
    # ownership of this contract to perform administrative actions.
    # The function takes as parameters:
    # - the new owner address
    # - the hashed newOwner address hashed and signed by the cold-wallet offline
    @sp.entry_point
    def changeOwner(self, params):
        sp.verify(sp.sender == self.data.ownerKey)
        messageToVerify = sp.pack(params.newOwnerKey)
        sp.verify(sp.check_signature(self.data.coldOwnerKey,
                                     params.signedMessage, messageToVerify))
        self.data.ownerKey = params.newOwnerKey

    # The Tezos account funding function is callable by anyone, and will update the
    # TBigMap container with the new balance for the account specified as a parameter.
    # The function takes as parameters:
    # - the public-key hash of the account being funded
    @sp.entry_point
    def feedUserAccount(self, params):
        self.initializeUserAccount(params.userPublicKeyHash)
        self.data.userToWallet[params.userPublicKeyHash].balance += sp.amount

    # The sendFunds function allows the user to spend tezos coins available in his
    # balance. The function requires however to be called by the owner of the contract
    # and the user must provide an unique message signed with his private key to
    # authorize the transfer of funds.
    # The message sent by the user must wrap and sign a nonce which is required
    # against replay attacks.
    # The function takes as parameters:
    # - the public-key of the account to modify
    # - the amount of coins the user wants to transfer
    # - the recipient address of the transfer
    # - the message signed by the user private key
    # - the fee amount the owner is paying when calling the contract
    @sp.entry_point
    def sendFunds(self, params):
        pKeyHash = sp.hash_key(params.userPublicKey)
        sp.verify(sp.sender == self.data.ownerKey)
        sp.verify(self.data.userToWallet.contains(pKeyHash))
        messageToVerify = sp.pack(sp.record(
            r=params.receiver, a=params.amount, c=self.data.userToWallet[pKeyHash].nonce))
        sp.verify(sp.check_signature(params.userPublicKey,
                                     params.signedMessage, messageToVerify))
        sp.verify(self.data.userToWallet[pKeyHash].balance >= (
            params.amount + params.feeAmount))
        sp.verify((sp.balance - (params.amount + params.feeAmount))
                  >= sp.mutez(0))
        self.data.userToWallet[pKeyHash].balance -= (
            params.amount + params.feeAmount)
        self.data.userToWallet[pKeyHash].nonce += 1
        sp.send(params.receiver, params.amount)
        sp.send(self.data.ownerKey, params.feeAmount)

    # The initializeUserAccount function is used internally by the contract and
    # checks if a user entry is already registered inside the contract
    # big_map data structure.
    # If the entry doesn't exist, it is created with default values.
    # The function takes as a parameter:
    # - the public-key of the account to check
    def initializeUserAccount(self, userPublicKeyHash):
        sp.if ~(self.data.userToWallet.contains(userPublicKeyHash)):
            # If it doesn't exist, we set a new entry with empty balance and active state
            self.data.userToWallet[userPublicKeyHash] = sp.record(
                balance=sp.tez(0),
                nonce=0)

    @sp.add_test(name="EasyWalletTesting")
    def test():
        scenario = sp.test_scenario()
        scenario.h1("EasyWallet contract testing")
        scenario.h2("Initializing contract")
        # Creating test accounts:
        firstOwner = sp.test_account("smart-chain")
        secondOwner = sp.test_account("globalPOS")
        coldWallet = sp.test_account("cold-wallet")
        # test addresses
        testAddress1 = sp.address("tz1g7nf5jwz4Cxo3J5FZ1CHUXZJDZLSoqRVG")
        testAddress2 = sp.address("tz1N1itLKdNMGtZJVHnX4JeEe1D5MZqZoGA2")

        # Setting smart-chain as creator of the contract:
        c1 = EasyWallet(firstOwner.address, coldWallet.public_key)
        scenario += c1

        scenario.h2("Transferring ownership")
        scenario.p("Giving ownership to secondOwner tezos account:")
        packedMessage = sp.pack(testAddress1)
        signatureTest = sp.make_signature(secret_key=coldWallet.secret_key,
                                          message=packedMessage,
                                          message_format='Raw')
        scenario += c1.changeOwner(newOwnerKey=testAddress1,
                                   signedMessage=signatureTest).run(sender=firstOwner)

        scenario.h2("Funding user accounts")
        scenario.p("Funding secondOwner user account with 1 xtz:")
        scenario += c1.feedUserAccount(userPublicKeyHash=secondOwner.public_key_hash).run(sender=secondOwner,
                                                                                          amount=sp.tez(1))
        # The balance of the user account should be updated:
        scenario.verify(
            c1.data.userToWallet[secondOwner.public_key_hash].balance > sp.tez(0))
        scenario.p("Funding secondOwner user account with 6 xtz:")
        scenario += c1.feedUserAccount(userPublicKeyHash=secondOwner.public_key_hash).run(sender=secondOwner,
                                                                                          amount=sp.tez(6))
        scenario.p("Funding firstOwner user account with 3 xtz:")
        scenario += c1.feedUserAccount(userPublicKeyHash=firstOwner.public_key_hash).run(sender=secondOwner,
                                                                                         amount=sp.tez(3))
        scenario.verify(
            c1.data.userToWallet[firstOwner.public_key_hash].balance > sp.tez(0))

        scenario.h2("Sending funds from the EasyWallet contract")
        scenario.p("Generating a transfer from the secondOwner user account:")
        # Creating a signed message authorizing the fund transfer
        packedMessage = sp.pack(sp.record(r=testAddress1,
                                          a=sp.mutez(1000),
                                          c=c1.data.userToWallet[secondOwner.public_key_hash].nonce))
        signatureTest = sp.make_signature(secret_key=secondOwner.secret_key,
                                          message=packedMessage,
                                          message_format='Raw')
        # Sending funds from the EasyWallet contract
        scenario += c1.sendFunds(userPublicKey=secondOwner.public_key,
                                 amount=sp.mutez(1000),
                                 receiver=testAddress1,
                                 signedMessage=signatureTest,
                                 feeAmount=sp.mutez(9566)).run(sender=secondOwner)
        scenario.p(
            "Generating another transfer from the secondOwner user account:")
        # Creating another signed message authorizing the fund transfer
        # We set the nonce manually so that we can reuse the signed message afterward
        packedMessage_ = sp.pack(sp.record(r=testAddress1,
                                           a=sp.tez(5),
                                           c=1))
        signatureTest2 = sp.make_signature(secret_key=secondOwner.secret_key,
                                           message=packedMessage_,
                                           message_format='Raw')
        # Sending funds from the EasyWallet contract
        scenario += c1.sendFunds(userPublicKey=secondOwner.public_key,
                                 amount=sp.tez(5),
                                 receiver=testAddress1,
                                 signedMessage=signatureTest2,
                                 feeAmount=sp.mutez(9566)).run(sender=secondOwner)

        scenario.h2("Tests against Replay Attacks")
        scenario.p(
            "Trying to reuse the same signed message should be blocked thanks to the nonce check:")
        scenario += c1.sendFunds(userPublicKey=secondOwner.public_key,
                                 amount=sp.tez(1),
                                 receiver=testAddress1,
                                 signedMessage=signatureTest2,
                                 feeAmount=sp.mutez(9566)).run(sender=secondOwner, valid=False)

        scenario.h2("Generating transfer with the wrong secret key")
        scenario.p(
            "Generating a transfer for a user account with the wrong private-key should be blocked:")
        packedMessage_ = sp.pack(sp.record(r=testAddress1,
                                           a=sp.tez(1),
                                           c=c1.data.userToWallet[secondOwner.public_key_hash].nonce))
        signatureTest_E = sp.make_signature(firstOwner.secret_key,
                                            packedMessage_,
                                            message_format='Raw')
        scenario += c1.sendFunds(userPublicKey=secondOwner.public_key,
                                 amount=sp.tez(1),
                                 receiver=testAddress1,
                                 signedMessage=signatureTest_E,
                                 feeAmount=sp.mutez(9566)).run(sender=secondOwner, valid=False)

        scenario.h2("Generating transfer with inadequate balance")
        scenario.p(
            "Generating a transfer with a sending amount greater than the available user account balance should be blocked:")
        packedMessage_U = sp.pack(sp.record(r=testAddress1,
                                            a=sp.tez(4),
                                            c=c1.data.userToWallet[firstOwner.public_key_hash].nonce))
        signatureTest_U = sp.make_signature(firstOwner.secret_key,
                                            packedMessage_U,
                                            message_format='Raw')
        scenario += c1.sendFunds(userPublicKey=firstOwner.public_key,
                                 amount=sp.tez(4),
                                 receiver=testAddress1,
                                 signedMessage=signatureTest_U,
                                 feeAmount=sp.mutez(9566)).run(sender=secondOwner, valid=False)

        scenario.h2("Contract interactions without administrative privileges")
        scenario.p(
            "Sending transfer requests without owner privileges should be blocked, even though the signed message is valid:")
        packedMessage_S = sp.pack(sp.record(r=testAddress1,
                                            a=sp.tez(1),
                                            c=c1.data.userToWallet[secondOwner.public_key_hash].nonce))
        signatureTest_S = sp.make_signature(firstOwner.secret_key,
                                            packedMessage_S,
                                            message_format='Raw')
        scenario += c1.sendFunds(userPublicKey=firstOwner.public_key,
                                 amount=sp.tez(1),
                                 receiver=testAddress1,
                                 signedMessage=signatureTest_S,
                                 feeAmount=sp.mutez(9566)).run(sender=firstOwner, valid=False)
        scenario.p(
            "Modifying the ownership of the contract should be blocked if the sender isn't the current owner:")
        packedMessage_E = sp.pack(testAddress2)
        signatureTest_E = sp.make_signature(secret_key=coldWallet.secret_key,
                                            message=packedMessage_E,
                                            message_format='Raw')
        scenario += c1.changeOwner(newOwnerKey=testAddress2,
                                   signedMessage=signatureTest_E).run(sender=firstOwner, valid=False)
        scenario.p("Modifying the ownership of the contract should be blocked if the sender doesn't sign the validation message with the cold-wallet private-key:")
        packedMessage_E = sp.pack(testAddress2)
        signatureTest_E = sp.make_signature(secret_key=firstOwner.secret_key,
                                            message=packedMessage_E,
                                            message_format='Raw')
        scenario += c1.changeOwner(newOwnerKey=testAddress2,
                                   signedMessage=signatureTest_E).run(sender=secondOwner, valid=False)
