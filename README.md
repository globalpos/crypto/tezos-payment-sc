# EasyWallet Smart-Contract

This repository contains the smart-contract used for the EasyWallet mobile application built with [SmartPy](http://smartpy.io/), a Python library to build Tezos smart-contracts.

## Requirements

- [docker](https://www.docker.com/)
- [docker-compose](https://docs.docker.com/compose/install/)

## How it works

The entry point is `docker-compose.yaml`, see the
[official documentation](https://docs.docker.com/compose/overview/) for more information.  
This file describes the global infrastructure, .ie all the services required to
run properly the project.

A `Makefile` provides some shortcuts to help you. Simply run the `make` command to see the help menu.

## Installation

To install the projet, one command is require:

```bash
make infra-up
```

This will launch a docker container with all the required dependencies.

## Compiling and test the smart-contract

Once the docker container is successfully launched, you can interact with it to compile and run the EasyWallet smart-contract.

### Compiling

To compile it, you will need to provide values for the constructor, with two parameters required:

- The owner address / Public-key hash.
- The public-key of the second owner, which should be a cold-wallet for security measures.

```bash
$ make smartpy-compile SCRIPT=EasyWalletContract/EasyWallet.py CALL=EasyWallet\(initialMasterKey=sp.address\(\\\"tz1hWzjK62ft3jufvrua6zfT3sLVmo7776kq\\\"\),coldOwnerPublicKey=sp.key\(\\\"edpkusYkvo99KWxnJp5ve6UGnrnBjYVX5HJp7WSd2y3CRZU1chaQNL\\\"\)\)
```

If you want to interact with the smart-contract directly from inside the container to avoid the `CALL` parameters formatting:

```bash
$ make infra-shell-tools
```

Followed by:

```bash
$ SmartPy.sh compile contract/EasyWallet.py 'EasyWallet(initialMasterKey="tz1hWzjK62ft3jufvrua6zfT3sLVmo7776kq",coldOwnerPublicKey="edpkusYkvo99KWxnJp5ve6UGnrnBjYVX5HJp7WSd2y3CRZU1chaQNL")' smartpy_output/EasyWallet
```

### Testing

Once the smart-contract is properly compiled, you can run the tests:

```bash
$ make smartpy-test SCRIPT=EasyWalletContract/EasyWallet.py CALL=EasyWallet\(initialMasterKey=sp.address\(\\\"tz1hWzjK62ft3jufvrua6zfT3sLVmo7776kq\\\"\),coldOwnerPublicKey=sp.key\(\\\"edpkusYkvo99KWxnJp5ve6UGnrnBjYVX5HJp7WSd2y3CRZU1chaQNL\\\"\)\)
```

## Smart-contract Entrypoints

The Smart-contract has 3 entrypoints available:

| Entrypoint  | Description | Parameters  | Permission |
|:---:|---|---|:---:|
| **feedUserAccount** | Funding function callable by anyone, which will update the TBigMap container with the new balance for the account specified as a parameter. |`userPublicKeyHash`: address  | Public |
| **sendFunds** | Allow the user to spend tezos coins available in its balance. The function requires however to be called by the owner of the contract and the user must provide an unique message generated with its balance nonce signed with his private key to authorize the transfer of funds. |`userPublicKey`: key / `amount`: int / `receiver`: address / `signedMessage`: signature / `feeAmount`: int | Owner |
| **changeOwner** | Allow the creator of the contract to transfer the ownership of this contract to perform administrative actions. |`newOwnerKey`: address / `signedMessage`: string | Owner |

Another function is available inside the smart-contract, **initializeUserAccount()** and is only used internally to check if a user entry is already registered inside the contract big_map data structure. If the user entry doesn't exist, it is created with default values.

## Deploy and interact with the smart-contract

For more details on the deployment and how to interact with the smart-contract with the tezos-client, please check the [Deployment documentation](docs/Deployment.md)

## Useful links

- [https://smartpy.io/dev/](https://smartpy.io/dev/)
- [https://smartpy.io/dev/reference.html](https://smartpy.io/dev/reference.html) 
- [https://mainnet.smartpy.io/](https://mainnet.smartpy.io/)
- [ecadlabs/taquito](https://github.com/ecadlabs/taquito)
