# Deployment and interactions

This documentation describes the differents step on how to deploy the EasyWallet smart-contract and interactwith it through the tezos client.

## Requirements

- [tezos-client](https://tezos.gitlab.io/api/cli-commands.html/)

## Michelson

First, compile the EasyWallet smart-contract with the process detailled [here](../README.md).

Once you compiled the smart-contract, you should have inside the `smartpy_output` folder two files:

- `EasyWallet_compiled.tz`
- `EasyWallet_storage_init.tz`

### Originate

Now, you can originate the contract, copy paste the `EasyWalletStorage.tz` to the --init flag:

```bash
tezos.client originate contract easyWallet transferring 0 from faucetWallet running EasyWallet_compiled.tz --init '(Pair "<cold_wallet_public_key>" (Pair "<owner_tezos_address>" {}))'  --burn-cap 3.014
```

## Contract calls

### FeedUserAccount

```bash
tezos.client transfer 1 from faucetWallet to <deployed_contract_address> --entrypoint 'feedUserAccount' --arg '"<recipient_tezos_address>"' --burn-cap 0.074
```

### ChangeOwner

```bash
tezos.client hash data '"<new_tezos_address_owner>"' of type address
```

Take the "Raw packed data"

```bash
tezos.client sign bytes '<raw_packed_data>' for coldWallet
```

```bash
tezos.client transfer 0 from faucetWallet to <deployed_contract_address> --entrypoint 'changeOwner' --arg 'Pair "<new_tezos_address_owner>" "<signed_message>"'
```

### Sendfunds

Preparation of the signature:

```michelson
Pair 1000000 (Pair 0 "<tezos_recipient_address>")
```

Where `1000000` is the amount to send in mutez and `0` is the nonce.

Use the RPC end point pack_data to pack the Michelson as Micheline expression:

POST chains/main/blocks/head/helpers/scripts/pack_data

```json
{
  "data": {
    "args": [
      {
        "int": "1000000"
      },
      {
        "args": [
          {
            "int": "0"
          },
          {
            "string": "<tezos_recipient_addres>"
          }
        ],
        "prim": "Pair"
      }
    ],
    "prim": "Pair"
  },
  "type": {
    "args": [
      {
        "prim": "mutez"
      },
      {
        "args": [
          {
            "prim": "int"
          },
          {
            "prim": "address"
          }
        ],
        "prim": "pair"
      }
    ],
    "prim": "pair"
  }
}
```

Sign the binary string output with the sender secret key:

```bash
tezos.client sign bytes '<bytes_to_sign>' for faucetWallet
```

Inject the operation:

```bash
tezos.client transfer 0 from faucetWallet to <deployed_contract_address> --entrypoint 'sendFunds' --arg 'Pair (Pair 1000000 1000) (Pair "<tezos_recipient_address>" (Pair "<signed_message>" "<public_key_sender>"))'
```
